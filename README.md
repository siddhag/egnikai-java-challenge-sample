#Egnikai Java Sample

This repository contains sample problems for the Egnikai system in the Java8 platform standard.


#Problems

## Prime Number Checker
Given a number checks whether it's prime or not.

*Contains hidden tests*

## Number Sum
Given two numbers, adds them up.

##Dev Tasks
*whenever new problem and related test cases are added or new source/test files are added*
*update https://bitbucket.org/siddhag/egnikai-springback/src//main/resources/data.sql*